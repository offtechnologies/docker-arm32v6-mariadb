# Raspberry Pi 1 MariaDB Docker Image With Alpine Linux

[![pipeline status](https://gitlab.com/offtechnologies/docker-arm32v6-mariadb/badges/master/pipeline.svg)](https://gitlab.com/offtechnologies/docker-arm32v6-mariadb/commits/master)
[![This image on DockerHub](https://img.shields.io/docker/pulls/offtechnologies/docker-arm32v6-mariadb.svg)](https://hub.docker.com/r/offtechnologies/docker-arm32v6-mariadb/)
[![](https://images.microbadger.com/badges/version/offtechnologies/docker-arm32v6-mariadb.svg)](https://microbadger.com/images/offtechnologies/docker-arm32v6-mariadb "Get your own version badge on microbadger.com")
[![](https://images.microbadger.com/badges/image/offtechnologies/docker-arm32v6-mariadb.svg)](https://microbadger.com/images/offtechnologies/docker-arm32v6-mariadb "Get your own image badge on microbadger.com")


[offtechurl]: https://gitlab.com/offtechnologies

[![offtechnologies](https://gitlab.com/offtechnologies/logos/raw/master/logo100.png)][offtechurl]


Raspberry Pi 1 compatible Docker image with Alpine Linux and MariaDB. Just ~42MB. It is based on the official MariaDB Docker Image ported to the arm32v6 based Raspbery Pi 1. The setup script is based on [official MariaDB docker image](https://hub.docker.com/_/mariadb/), so the usage is the same.
